<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>
        Buat Account Baru!
    </h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
        <p>First name:</p>
        <input type="text" name="first_name">
        <p>Last name:</p>
        <input type="text" name="last_name">
        <br><br>
        <label>Gender:</label>
        <br><br>
        <input type="Radio" name="gender" value="0">Male<br>
        <input type="Radio" name="gender" value="1">Female<br>
        <input type="Radio" name="gender" value="2">Other<br><br>
        <label>Nationalty:</label>
        <br>
            <select>
                <option>Arab</option>
                <option>Indonesia</option>
                <option>Malaysia</option>
                <option>Singapurat</option>
            </select>
            <br><br>
        <label>Language Spoken:</label>
        <br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>
        <label>Bio:</label>
        <br><br>
        <textarea name="" id="" cols="40" rows="12"></textarea>
        <br>
        <input type="Submit" value="Sign Up">
    </form>
    <br><br><br>
   <!--Dibuat Oleh Restu Aji Nugroho-->
   <footer>From Restu • Build with LOVE!!! •</footer>
</body>
</html>